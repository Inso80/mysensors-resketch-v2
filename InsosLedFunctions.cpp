// 
// 
// 

#include "InsosLedFunctions.h"

InsosLedFunctions::InsosLedFunctions(byte * anyMillisProgramRunning, unsigned long * millisDelayTimeDefault, unsigned long * startMillis) :	anyMillisProgramRunning{ anyMillisProgramRunning }, millisDelayTimeDefault{ millisDelayTimeDefault }, startMillis{ startMillis }
{
	#ifdef LEDS2812
	FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS);	// LEDs werden erstellt
	#endif

	#ifdef LEDS2812_ZWEI_PINS
	FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, 0, NUM_LEDS_PT_TWO);	// LEDs werden erstellt
	FastLED.addLeds<WS2812B, LED_PIN_2, GRB>(leds, NUM_LEDS_PT_TWO, NUM_LEDS_PT_TWO);	// LEDs werden erstellt
	#endif

	#ifdef LEDS2811_ZWEI_PINS
	FastLED.addLeds<WS2811, LED_PIN, BRG>(leds, 0, NUM_LEDS_PT_TWO);
	FastLED.addLeds<WS2811, LED_PIN_2, BRG>(leds, NUM_LEDS_PT_TWO, NUM_LEDS_PT_TWO);
	#endif

	FastLED.setBrightness(255); // Helligkeit einmalig festsetzen

	fill_solid(leds, NUM_LEDS, CRGB(0, 0, 0));	// alles auf schwarz (falls noch LEDs an sind)

	FastLED.show();	// und alles wird eingeschaltet

	createArraysOnObjectCreation();
}

void InsosLedFunctions::createArraysOnObjectCreation()
{
	for (int r = 0; r < 3; ++r) {
		redActu[r] = 0;
		greenActu[r] = 0;
		blueActu[r] = 0;

		redNext[r] = 0;
		greenNext[r] = 0;
		blueNext[r] = 0;

		// millisDelayTimeDefault[r] = 0;
		// startMillis[r] = 0;

		stepValueTemp[r] = 0;
		stepNow[r] = 0;
		singleLedWhichActual[r] = 0;

		longTimeDimStep[r] = 0;

		//laufLichtStepsCount[r] = 0;

		//laufLichtStepsLimit[r] = 0;

		dimTempValues[r] = 0;

		emptyStepsOnOffDelay[r] = 0;

		// startOfStripeLoop[r] = 0;
		// endOfStripeLoop[r] = 0;

		byteToDelayDim[r] = 0;

		for (int s = 0; s < 3; ++s) {
			amountOfSteps[r][s] = 0;
			counterForFullsteps[r][s] = 0;
			actualState[r][s] = 0;

			rgbSetActu[r][s] = 0;
			rgbSetNext[r][s] = 0;
		}
	}

	startOfStripe[0] = 0;
	endOfStripe[0] = FIRST_PART_LEDS - 1;

	startOfStripe[1] = FIRST_PART_LEDS;
	endOfStripe[1] = FIRST_PART_LEDS + SECOND_PART_LEDS - 1;

	startOfStripe[2] = FIRST_PART_LEDS + SECOND_PART_LEDS;
	endOfStripe[2] = NUM_LEDS - 1;
}


/*
program modes:
0: off
1: on	mit ms

anymillisprogramrunning:

5 = laufLicht
/
 WICHTIG: die dimTime ist flexibel. Bei wakeup und Sleeplight sind es Minuten, ansonsten millisekunden. wird dann hier ggf entsprechend des programs umgerechnet
 */
void InsosLedFunctions::callAnyProgram(byte which, byte programMode = 1, byte red = 0, byte green = 0, byte blue = 0, unsigned long dimTime = DIM_TIME_DEFAULT, bool additionalBool = 0) {
	/*#ifdef DEBUG
	Serial.println(" ");
	Serial.println("Call Any Program");
	Serial.println(" ");
	Serial.print("which = ");
	Serial.println(which);
	Serial.print("programMode = ");
	Serial.println(programMode);
	Serial.print("red = ");
	Serial.println(red);
	Serial.print("green = ");
	Serial.println(green);
	Serial.print("blue = ");
	Serial.println(blue);
	Serial.print("dimTime = ");
	Serial.println(dimTime);
	Serial.print("stepNow = ");
	Serial.println(stepNow[which]);
	Serial.println(" ");
	Serial.println(" ");
	Serial.println(" - - - - - - - - - - - - ");
	Serial.println(" ");
	#endif*/

	if (which >= 0 && which < anzahlAbschnitte) {	// damit nicht auf Bereiche zugegriffen werden kann, die ausserhalb der Arrays liegen
		if (programMode < 10) {	// alles bis 10 sind "feste" programme, danach kommt bspw so was wie nen Farbwechsel oder Festprogramm das keine rgbs ben�tigt
			if (redActu[which] != red || greenActu[which] != green || blueActu[which] != blue) {	// falls eine �nderung auftritt
				if (programMode == 1) {		// normales einschalten mit ms
					if (red == 0 && green == 0 && blue == 0) {	// falls am Ende aus
						if (stepNow[which] == 10) {	// wenn Ausgangspunkt: alle an
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, dimTime, 1);
							dimFromToInitialWhenOffOneByOneFollows_ModeEverySecond(which);
							xHelperEinzelnOnOffRgbPreCalc(which, 0);
							xHelperSetNext(dimTempValues[0], dimTempValues[1], dimTempValues[2], which);
						}
						else {	// falls man noch im Zwischenschritt steckt
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, dimTime, 1);
							ledsOffOneByOneInitial(which);
						}
					}
					else { // falls am Ende irgendeine Farbe min gr�sser 1
						if (stepNow[which] == 10) {	// wenn Ausgangspunkt: alle an
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, dimTime, 0);
							dimFromToInitial_ModeEverySecond(which);
						}
						else {
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, dimTime, 1);
							ledsOnOneByOneInitial(which);
						}
					}
				}
				else if (programMode == 2) {	// wake up light
					if (stepNow[which] == 10) {	// wenn Ausgangspunkt: alle an
						xHelperSetNext(red, green, blue, which);
						xHelperCalcDefaultDelayTimePerStep(which, xHelperCalcMinutesToMs(dimTime), 0, 1, 10);
						dimFromToInitial_ModeEveryTenth(which);
					}
					else {
						xHelperSetNext(red, green, blue, which);
						xHelperCalcDefaultDelayTimePerStep(which, xHelperCalcMinutesToMs(dimTime), 1, 1, 10);
						ledsOnOneByOneInitial(which, 1);
					}

				}
				else if (programMode == 3) {	// sleep light mit �bergabe als Minuten und automatischer Umrechnung
					if (redActu[which] != 0 || greenActu[which] != 0 || blueActu[which] != 0) {	// falls �berhaupt Licht an ist
						if (stepNow[which] == 10) {	// wenn Ausgangspunkt: alle an
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, xHelperCalcMinutesToMs(dimTime), 1, 1, 10);
							dimFromToInitialWhenOffOneByOneFollows_ModeEveryTenth(which);
							xHelperEinzelnOnOffRgbPreCalc(which, 0);
							xHelperSetNext(dimTempValues[0], dimTempValues[1], dimTempValues[2], which);
							//dimFromToInitial(which);
						}
						else {	// falls man noch im Zwischenschritt steckt
							xHelperSetNext(red, green, blue, which);
							xHelperCalcDefaultDelayTimePerStep(which, xHelperCalcMinutesToMs(dimTime), 1, 1, 10);
							ledsOffOneByOneInitial(which, 1);
						}
					}
				}

			}
		}
		else {
			if (programMode == 41) { // reihe nach an vorw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsJedeFuerSichInitial(which);
			}
			else if (programMode == 42) { // reihe nach an r�ckw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsJedeFuerSichInitial(which);
			}
			else if (programMode == 43) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsGesamtesArrayInitial();
			}
			else if (programMode == 44) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsGesamtesArrayInitial();
			}
			else if (programMode == 45) { // reihe nach an vorw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsJedeFuerSichInitial(which, 1);
			}
			else if (programMode == 46) { // reihe nach an r�ckw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsJedeFuerSichInitial(which, 1);
			}
			else if (programMode == 47) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsGesamtesArrayInitial(1);
			}
			else if (programMode == 48) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsGesamtesArrayInitial(1);
			}
			else if (programMode == 49) { // reihe nach an vorw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsJedeFuerSichInitial(which, 2);
			}
			else if (programMode == 50) { // reihe nach an r�ckw�rts einzelne Stripes
				xHelperSetNext(red, green, blue, which);
				millisDelayTimeDefault[which] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsJedeFuerSichInitial(which, 2);
			}
			else if (programMode == 51) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnVorwaertsGesamtesArrayInitial(2);
			}
			else if (programMode == 52) { // reihe nach an r�ckw�rts gesamtes Array
				for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) xHelperSetNext(red, green, blue, x);
				millisDelayTimeDefault[0] = LAUF_LICHT_DELAY_TIME;
				reiheNachAnRueckwaertsGesamtesArrayInitial(2);
			}
		}
	}
}

void InsosLedFunctions::runAnyProgram(byte which) {
	/*#ifdef DEBUG
	Serial.println(" ");
	Serial.println("runAnyProgram called");
	Serial.println(" ");
	xHelperPrintSteps(which);
	Serial.println(" ");
	#endif*/
	if (anyMillisProgramRunning[which] == 1) {					// sobald ledsOnOneByOneInitial durchgelaufen ist
		if (stepNow[which] != 10) ledsOnOneByOneShortLoop(which);	// den loop einmal rennen lassen = stepow voll (10)
		else dimFromToInitial_ModeEverySecond(which);							// danach den Initiator vom Hochdimmen starten, damit wird anyMillisProgramRunning[which] 3 gesetzt
	}
	else if (anyMillisProgramRunning[which] == 2) {					// nachdem OffOneByOneInitial durchlaufen wurde
		if (stepNow[which] != 0) ledsOffOneByOneShortLoop(which);
		else resetLoopBools(which);
	}
	else if (anyMillisProgramRunning[which] == 3) {					// dim from to loop
		if (longTimeDimStep[which] != 0) xHelperDim_ModeEverySecond(which);	// falls alle Werte angeglichen wurde durch xHelperSetLedFromTo und jetzt die einzelnen angleichungen stattfinden sollen
		else if (xHelperSetLedFromTo(which));// xHelperSetLedFromTo(which);		// falls die Funktion true zur�ck gibt geht es mit xHelperSetLedFromTo(which) weiter, danach wieder 9x erstes if
		else if (redNext[which] == 0 && greenNext[which] == 0 && blueNext[which] == 0) ledsOffOneByOneInitial(which);	// falls nach dem Runterdimmen noch von 1 auf 0 soll
		else resetLoopBools(which);											// wenn alle Schritte durchluafen sind wird das Programm beendet
	}
	else if (anyMillisProgramRunning[which] == 4) {		// dim from to mit anschliessendem off one by one (hier auf eins statt auf null gedimmt..)
		if (longTimeDimStep[which] != 0) xHelperDim_ModeEverySecond(which);	// falls alle Werte angeglichen wurde durch xHelperSetLedFromTo und jetzt die einzelnen angleichungen stattfinden sollen
		else if (xHelperSetLedFromTo(which));// xHelperSetLedFromTo(which);		// falls die Funktion true zur�ck gibt geht es mit xHelperSetLedFromTo(which) weiter, danach wieder 9x erstes if
		else ledsOffOneByOneInitial(which);	// falls nach dem Runterdimmen noch von 1 auf 0 soll
	}
	else if (anyMillisProgramRunning[which] == 5) {	// vorwaerts loop, licht bleibt an
		reiheNachAnVorwaertsLoop(which);
	}
	else if (anyMillisProgramRunning[which] == 6) {	// r�ckw�rts loop, licht bleibt an
		reiheNachAnRueckwaertsLoop(which);
	}
	else if (anyMillisProgramRunning[which] == 7) {	// vorwaerts loop, nur lauflicht
		reiheNachAnVorwaertsLoop(which, 1);
	}
	else if (anyMillisProgramRunning[which] == 8) {	// r�ckw�rts loop, nur lauflicht
		reiheNachAnRueckwaertsLoop(which, 1);
	}
	else if (anyMillisProgramRunning[which] == 9) {	// vorwaerts loop, nur lauflicht, danach wieder urspr�ngliche Farbe
		reiheNachAnVorwaertsLoop(which, 2);
	}
	else if (anyMillisProgramRunning[which] == 10) {	// r�ckw�rts loop, nur lauflicht, danach wieder urspr�ngliche Farbe
		reiheNachAnRueckwaertsLoop(which, 2);
	}
	else if (anyMillisProgramRunning[which] == 11) {					// sobald ledsOnOneByOneInitial durchgelaufen ist
		if (stepNow[which] != 10) ledsOnOneByOneLongLoop(which);	// den onebyone loop rennen lassen bis stepow voll (10) ist
		else dimFromToInitial_ModeEverySecond(which);							// danach den Initiator vom Hochdimmen starten, damit wird anyMillisProgramRunning[which] 3 gesetzt
	}
	else if (anyMillisProgramRunning[which] == 12) {					// nachdem OffOneByOneInitial durchlaufen wurde
		if (stepNow[which] != 0) ledsOffOneByOneLongLoop(which);
		else resetLoopBools(which);
	}
	else if (anyMillisProgramRunning[which] == 13) {					// wake up light mode
		if (longTimeDimStep[which] != 0) xHelperDim_ModeEveryTenth(which);	// falls alle Werte angeglichen wurde durch xHelperSetLedFromTo und jetzt die einzelnen angleichungen stattfinden sollen
		else if (xHelperSetLedFromTo(which));// xHelperSetLedFromTo(which);		// falls die Funktion true zur�ck gibt geht es mit xHelperSetLedFromTo(which) weiter, danach wieder 9x erstes if
		else if (redNext[which] == 0 && greenNext[which] == 0 && blueNext[which] == 0) ledsOffOneByOneInitial(which, 1);	// falls nach dem Runterdimmen noch von 1 auf 0 soll
		else resetLoopBools(which);											// wenn alle Schritte durchluafen sind wird das Programm beendet
	}
	else if (anyMillisProgramRunning[which] == 14) {		// Sleep light mode
		if (longTimeDimStep[which] != 0) xHelperDim_ModeEveryTenth(which);	// falls alle Werte angeglichen wurde durch xHelperSetLedFromTo und jetzt die einzelnen angleichungen stattfinden sollen
		else if (xHelperSetLedFromTo(which));// xHelperSetLedFromTo(which);		// falls die Funktion true zur�ck gibt geht es mit xHelperSetLedFromTo(which) weiter, danach wieder 9x erstes if
		else ledsOffOneByOneInitial(which, 1);	// falls nach dem Runterdimmen noch von 1 auf 0 soll
	}
	/*Serial.println("");
	Serial.print("stepNow[which] = ");
	Serial.println(stepNow[which]);*/
}


// Vorberechnung der Wartezeit beim Dimmen, egal ob up oder down, mit oder ohne einzeln an / aus (inclStepUpDown)
// setzt ebenfalls schon die calc- und durchlaufMenge-Werte
void InsosLedFunctions::xHelperCalcDefaultDelayTimePerStep(byte which, unsigned long delayTimeInMiliSeconds, byte inclStepUpDown, bool longOne = 0, byte dimStepsPerRun = 2)
{
	/*#ifdef DEBUG
	Serial.println(" ");
	Serial.println("xHelperCalcDefaultDelayTimePerStep");
	xHelperPrintActu(which);
	xHelperPrintNext(which);
	#endif*/
	rgbSetActu[which][0] = &redActu[which];
	rgbSetNext[which][0] = &redNext[which];
	rgbSetActu[which][1] = &greenActu[which];
	rgbSetNext[which][1] = &greenNext[which];
	rgbSetActu[which][2] = &blueActu[which];
	rgbSetNext[which][2] = &blueNext[which];
	/* Funktion von dimFromTo abgeleitet. Arbeitet jetzt mit Aufruf von ledsEinzelnAn / aus, alles �ber Millis und helper boolean der null wird wenn anderes Programm startet
	// EinzelnAnAus integration hier da die delay time erst nach den Steps festgelegt werden kann (dim = 10 steps)
	// risingLight sagt ob es ein wakeUp (1) oder sleepLight (0) ist
	// Abfrage ob �nderung �berhaupt stattfindet muss vor dem Helper Aufruf erfolgen !
	*/
	for (byte x = 0; x < 3; ++x) {	// alles wieder zur�ck auf null setzen
		amountOfSteps[which][x] = 0;
		counterForFullsteps[which][x] = 0;
		actualState[which][x] = 0;
	}
	for (byte i = 0; i < 3; ++i) {							// drei Durchl�ufe (f�r r g und b),Ermitteln der Steps
		int helperActu = *rgbSetActu[which][i];
		int helperNext = *rgbSetNext[which][i];
		if (helperActu < helperNext) {  // hochdimmen
			while (helperActu < helperNext) {
				helperActu += xHelperGetChangeValue(4, helperActu);
				++amountOfSteps[which][i];
			}
		}
		else {
			while (helperActu > helperNext) {
				helperActu -= xHelperGetChangeValue(4, helperActu);
				++amountOfSteps[which][i];
			}
		}
	}
	// Berechnung der Durchl�ufe entsprechend des gerade berechneten arrays
	// counterForFullSteps ist dabei der Anteil an Durchl�ufen f�r die weniger enthaltenen Werte. Hat bspw rot 10 und blau 5 durchl�ufe, ist der Wert 0,5 und blau wird nur jedes 2te mal ausgef�hrt
	byte r = 0;
	while (r < 3) {	//Maximal drei Durchl�ufe da max drei Steps, ggf Ausstieg vorher
		if (amountOfSteps[which][r] >= amountOfSteps[which][0] && amountOfSteps[which][r] >= amountOfSteps[which][1] && amountOfSteps[which][r] >= amountOfSteps[which][2]) {		// den gr��ten Wert finden
			for (byte s = 0; s < 3; ++s) {		// �ngleich _aller_ Counter. H�chster durch sich selbt ist 1, die anderen wenn �berhaupt vorhanden leiner als 1
				if (amountOfSteps[which][r] > amountOfSteps[which][s] && amountOfSteps[which][s] > 0) actualState[which][s] += 0.5; // falls es nicht der H�chstwertige Stripe ist und mindestens ein Schritt kommen wird, den Schrittz�hler um 0.5 hochsetzen, so das die �nderungen "mitti�g" ausgef�hrt werden
				if (amountOfSteps[which][s] != 0) counterForFullsteps[which][s] = (float)amountOfSteps[which][s] / amountOfSteps[which][r];	// Festsetzen des "Anteils" beim Hochdimmen, so das alle x Teile die Nebenfarbe hchgedimmt wird, bzw 1 f�r die Hauptfarbe(n)
			}
			xHelperCalcDefaultDelayTimeHelperOfHelper(which, delayTimeInMiliSeconds, inclStepUpDown, longOne, r, dimStepsPerRun);	// Berechnung der Delay Time anhand des h�chsten Wertes
			r = 3; // damit die Schleife aussteigt
		}
		else ++r;
	}
}

void InsosLedFunctions::xHelperCalcDefaultDelayTimeHelperOfHelper(byte which, unsigned long delayTimeInMiliSeconds, byte inclStepUpDown, bool longOne, byte whichIsBiggest, byte dimStepsPerRun) {
	byte anteilEinAus = 100;	// prozent Anteil dimm-prozess an Gesamtzeit
	if (inclStepUpDown) {
		if (amountOfSteps[which][whichIsBiggest] < 31) anteilEinAus = 66;
		else anteilEinAus = 80;
		delayTimeInMiliSeconds = (delayTimeInMiliSeconds / 100) * anteilEinAus;
	}
	millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / (amountOfSteps[which][whichIsBiggest] * dimStepsPerRun);

	//if (longOne == 1) {
	//	if (inclStepUpDown)		millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / ((amountOfSteps[which][whichIsBiggest] * 10) + (((endOfStripe[which] + 1) - startOfStripe[which]) * emptyStepsOnOffDelayIfNotDefault));
	//		//millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / ((amountOfSteps[which][whichIsBiggest] * 10) + (((endOfStripe[which] + 1) - startOfStripe[which]) * emptyStepsOnOffDelayIfNotDefault));// delayTimeInMinutesinMin (* 60000 i.e. min to millis) / (steps (*10 da nur jede 10te led je Step einzeln angesprochen wird) plus jede einzelLed 1 auf 0, dabei jedoch zehnnfache Zeit sonst zu schnell)
	//	else millisDelayTimeDefault[which] = ((delayTimeInMiliSeconds) / (amountOfSteps[which][whichIsBiggest] * 10));
	//}
	//else {
	//	if (inclStepUpDown)	
	//		millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / ((amountOfSteps[which][whichIsBiggest]) + (((endOfStripe[which] + 1) - startOfStripe[which])));// delayTimeInMinutesinMin (* 60000 i.e. min to millis) / (steps plus jede einzelLed 1 auf 0)
	//	else millisDelayTimeDefault[which] = ((delayTimeInMiliSeconds) / (amountOfSteps[which][whichIsBiggest]));
	//}
	#ifdef DEBUG
	xHelperPrintDelayCalculation(whichIsBiggest, inclStepUpDown, which, delayTimeInMiliSeconds);
	#endif;
}


// setzt bei einzeln an und aus die Zwischenfarben, pr�ft mit checkForRgbNext auf actu(0) oder next(1), kein direkter Aufruf, nur innerhalb der "Ober"funktionen
void InsosLedFunctions::xHelperEinzelnOnOffRgbPreCalc(byte whichStripe, boolean checkForRgbNext = 0) {	// whichColor 0 = red, 1 = green, 2 = blue, schreibt in "dimTempValues"
	byte * rgbSet[3];
	if (checkForRgbNext == 0) {	// check for Actu
		rgbSet[0] = &redActu[whichStripe];
		rgbSet[1] = &greenActu[whichStripe];
		rgbSet[2] = &blueActu[whichStripe];
	}
	else {	// check for next
		rgbSet[0] = &redNext[whichStripe];
		rgbSet[1] = &greenNext[whichStripe];
		rgbSet[2] = &blueNext[whichStripe];
	}
	// falls alle Farben schon auf 1 oder 0 sind einfach den aktuellen Wert zur�ckgeben
	// if (*rgbSet[0] <= 1 && *rgbSet[1] <= 1 && *rgbSet[2] <= 1) for (byte r = 0; r < 3; ++r) dimTempValues[r] = *rgbSet[r];	// falls die Werte 
	// hier wird h�chster Wert ermittelt. Dann wird dieser durch 2 geteilt. jeder der mindestens halb "so hell" ist wie der h�chste Wert wird 1 gesetzt (am Ende an)
	for (byte r = 0; r < 3; ++r) if (*rgbSet[r] >= *rgbSet[0] && *rgbSet[r] >= *rgbSet[1] && *rgbSet[r] >= *rgbSet[2]) { // if ist hier die abbruchbedingung, r wird danach 3 gesetzt
		for (byte s = 0; s < 3; ++s) {
			*rgbSet[r] / 2 <= *rgbSet[s] ? dimTempValues[s] = 1 : dimTempValues[s] = 0;
		}
		r = 3;
	}
}


// einzeln an aus
void InsosLedFunctions::ledsOnOneByOneInitial(byte which = 0, bool longOne = 0, byte emptyStepsOnOffDelayIfNotDefault = DELAY_DIM_VERZOEGERUNG_RUNS) {

	emptyStepsOnOffDelay[which] = emptyStepsOnOffDelayIfNotDefault;

	stepValueTemp[which] = 3;

	byteToDelayDim[which] = 0;

	if (stepNow[which] != 0 && stepNow[which] != 10) {
		--stepNow[which];	// einen Schritt "weiter vorn" anfangen, da ggf da unterbrochen wurde
		stepValueTemp[which] += (3 * stepNow[which]);		// wenn stepValueTemp[which], also der Schrittz�hler der �bergreifend mitz�hlt, "mittendrin" ist	
	}
	else {
		xHelperEinzelnOnOffRgbPreCalc(which, 1);	// die tempValues updaten
		stepNow[which] = 0;
	}
	while (stepValueTemp[which] > 9) stepValueTemp[which] -= 10;			// Wenn nach dem Angleich des Schrittz�hlers dieser 0-9 �bersteigt

	singleLedWhichActual[which] = stepValueTemp[which];

	if (longOne == 0) anyMillisProgramRunning[which] = 1;
	else anyMillisProgramRunning[which] = 11;
}

void InsosLedFunctions::ledsOnOneByOneLongLoop(byte which)
{
	if (byteToDelayDim[which] < emptyStepsOnOffDelay[which]) ++byteToDelayDim[which];
	else {
		while (singleLedWhichActual[which] < startOfStripe[which]) singleLedWhichActual[which] += 10;	// wird nur f�r den ersten Durchlauf gebraucht, ansonsten in schleife abgearbeitet
		while (singleLedWhichActual[which] > endOfStripe[which] && stepNow[which] < 10) {	// falls die einzelne zu setzende LED nicht existiert, man �ber das Array hinaus ist
			++stepNow[which];
			stepValueTemp[which] += 3;
			if (stepValueTemp[which] > 9) stepValueTemp[which] -= 10;
			singleLedWhichActual[which] = stepValueTemp[which];
			while (singleLedWhichActual[which] < startOfStripe[which]) singleLedWhichActual[which] += 10;
		}
		if (stepNow[which] != 10) {	// wird immer ausgef�hrt so lange nicht alle hochgedimmt wurden
			leds[singleLedWhichActual[which]] = CRGB(dimTempValues[0], dimTempValues[1], dimTempValues[2]);
			singleLedWhichActual[which] += 10;
		}
		else {
			xHelperSetActu(dimTempValues[0], dimTempValues[1], dimTempValues[2], which, 1);	// sobald die 10 Steps erreicht sind, i.e. alle an sind
		}
		byteToDelayDim[which] = 0;
	}
}

void InsosLedFunctions::ledsOnOneByOneShortLoop(byte which)
{
	while (singleLedWhichActual[which] > endOfStripe[which]) {	// falls die einzelne zu setzende LED nicht existiert, man �ber das Array hinaus ist
		++stepNow[which];
		stepValueTemp[which] += 3;
		if (stepValueTemp[which] > 9) stepValueTemp[which] -= 10;
		singleLedWhichActual[which] = stepValueTemp[which];
		while (singleLedWhichActual[which] < startOfStripe[which]) singleLedWhichActual[which] += 10;
	}
	if (stepNow[which] != 10) {	// wird immer ausgef�hrt so lange nicht alle hochgedimmt wurden
		while (singleLedWhichActual[which] <= endOfStripe[which]) {
			leds[singleLedWhichActual[which]] = CRGB(dimTempValues[0], dimTempValues[1], dimTempValues[2]);
			singleLedWhichActual[which] += 10;
		}
	}
	else xHelperSetActu(dimTempValues[0], dimTempValues[1], dimTempValues[2], which, 1);	// sobald die 10 Steps erreicht sind, i.e. alle an sind
}



void InsosLedFunctions::ledsOffOneByOneInitial(byte which, byte longOne = 0, byte emptyStepsOnOffDelayIfNotDefault = DELAY_DIM_VERZOEGERUNG_RUNS) {

	emptyStepsOnOffDelay[which] = emptyStepsOnOffDelayIfNotDefault;

	byteToDelayDim[which] = 0;

	stepValueTemp[which] = 0;

	if (stepNow[which] != 0 && stepNow[which] != 10) {
		++stepNow[which];	// einen Schritt "weiter vorn" anfangen, da ggf da unterbrochen wurde
		stepValueTemp[which] -= (3 * stepNow[which]);		// wenn stepValueTemp[which], also der Schrittz�hler der �bergreifend mitz�hlt, "mittendrin" ist	
	}
	else {
		// xHelperEinzelnOnOffRgbPreCalc(which, 0);	// die tempValues updaten
		stepNow[which] = 10;
	}

	while (stepValueTemp[which] > 9) stepValueTemp[which] -= 10;			// Wenn nach dem Angleich des Schrittz�hlers dieser 0-9 �bersteigt

	singleLedWhichActual[which] = stepValueTemp[which];

	if (longOne == 0) anyMillisProgramRunning[which] = 2;
	else anyMillisProgramRunning[which] = 12;
}

void InsosLedFunctions::ledsOffOneByOneLongLoop(byte which)
{
	if (byteToDelayDim[which] < emptyStepsOnOffDelay[which]) ++byteToDelayDim[which];
	else {
		while (singleLedWhichActual[which] < startOfStripe[which]) singleLedWhichActual[which] += 10;	// wird nur f�r den ersten Durchlauf gebraucht, ansonsten in schleife abgearbeitet
		while (singleLedWhichActual[which] > endOfStripe[which]) {	// falls die einzelne zu setzende LED nicht existiert
			--stepNow[which];
			stepValueTemp[which] -= 3;
			if (stepValueTemp[which] < 0) stepValueTemp[which] += 10;
			singleLedWhichActual[which] = stepValueTemp[which];
			while (singleLedWhichActual[which] < startOfStripe[which]) singleLedWhichActual[which] += 10;
		}
		if (stepNow[which] != 0) {
			leds[singleLedWhichActual[which]] = CRGB(0, 0, 0);
			singleLedWhichActual[which] += 10;
		}
		else xHelperSetActu(0, 0, 0, which, 1);	// sobald die 10 Steps erreicht sind, i.e. alle an sind
		byteToDelayDim[which] = 0;
	}
}

void InsosLedFunctions::ledsOffOneByOneShortLoop(byte which)	// hier wird direkt jede zehnte runtegesetzt. bei 23 LEDs bspw die 1 und 11 in einem Rutsch
{
	while (singleLedWhichActual[which] > endOfStripe[which]) {	// falls die einzelne zu setzende LED nicht existiert
		--stepNow[which];
		stepValueTemp[which] -= 3;
		while (stepValueTemp[which] < 0) stepValueTemp[which] += 10;
		singleLedWhichActual[which] = stepValueTemp[which];
	}
	if (stepNow[which] != 0) {
		while (singleLedWhichActual[which] <= endOfStripe[which]) {
			leds[singleLedWhichActual[which]] = CRGB(0, 0, 0);
			singleLedWhichActual[which] += 10;
		}
	}
	else xHelperSetActu(0, 0, 0, which, 1);	// sobald die 10 Steps erreicht sind, i.e. alle an sind
}



void InsosLedFunctions::dimFromToInitial_ModeWholeStripe(byte which) {	// die ganze Stripe wird in einem Schritt gedimt
	longTimeDimStep[which] = 0;												// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {		// falls noch ein Schritt mindestens n�tig ist (funktion returned 1) wird der Program Mode auf 3 gesetzt und es geht damit weiter
		anyMillisProgramRunning[which] = 23;
	}
	else resetLoopBools(which);												// falls nicht wird das Programm beendet
}

void InsosLedFunctions::dimFromToInitial_ModeEverySecond(byte which) {	// die ganze Stripe wird in zwei Schritten (0,2,4,6,8 bspw) gedimt
	longTimeDimStep[which] = 0;												// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {		// falls noch ein Schritt mindestens n�tig ist (funktion returned 1) wird der Program Mode auf 3 gesetzt und es geht damit weiter
		anyMillisProgramRunning[which] = 3;
	}
	else resetLoopBools(which);												// falls nicht wird das Programm beendet
}

void InsosLedFunctions::dimFromToInitial_ModeEveryTenth(byte which) {			// die ganze Stripe wird in 10 Schritten gedimt (wakeUp / sleepLight)
	longTimeDimStep[which] = 0;												// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {		// falls noch ein Schritt mindestens n�tig ist (funktion returned 1) wird der Program Mode auf 3 gesetzt und es geht damit weiter
		anyMillisProgramRunning[which] = 13;
	}
	else resetLoopBools(which);												// falls nicht wird das Programm beendet
}


void InsosLedFunctions::dimFromToInitialWhenOffOneByOneFollows_ModeWholeStripe(byte which) {
	longTimeDimStep[which] = 0;				// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {
		anyMillisProgramRunning[which] = 24;
	}
	else resetLoopBools(which);		// falls nicht wird das Programm beendet
}

void InsosLedFunctions::dimFromToInitialWhenOffOneByOneFollows_ModeEverySecond(byte which) {
	longTimeDimStep[which] = 0;				// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {
		anyMillisProgramRunning[which] = 4;
	}
	else resetLoopBools(which);		// falls nicht wird das Programm beendet
}

void InsosLedFunctions::dimFromToInitialWhenOffOneByOneFollows_ModeEveryTenth(byte which) {
	longTimeDimStep[which] = 0;				// zur�cksetzen, da ggf das letze mal mitten drin unterbrochen wurde
	if (xHelperSetLedFromTo(which)) {
		anyMillisProgramRunning[which] = 14;
	}
	else resetLoopBools(which);		// falls nicht wird das Programm beendet
}


bool InsosLedFunctions::xHelperSetLedFromTo(byte which, boolean every = 1)
{
	bool returnBool{ 0 };
	// Actu um einen Step erh�hen / verringern bei jeder Farbe
	if (amountOfSteps[which][0] != 0 || amountOfSteps[which][1] != 0 || amountOfSteps[which][2] != 0)
	{
		returnBool = 1;
		// Aufrechnung : Hauptwert +1 (l�uft jede Runde), die andere +0.x, sobald da 1 ein durchlauf und -1
		for (byte i = 0; i < 3; ++i) if (amountOfSteps[which][i] != 0) actualState[which][i] += counterForFullsteps[which][i];
		for (byte i = 0; i < 3; ++i) {
			if (actualState[which][i] >= 1 && amountOfSteps[which][i] > 0) {
				byte changeOfColor = xHelperGetChangeValue(i + 1, 0, which);
				if (*rgbSetActu[which][i] > *rgbSetNext[which][i]) {	// rgb nimmt ab
					if ((*rgbSetActu[which][i] - changeOfColor) < *rgbSetNext[which][i]) *rgbSetActu[which][i] = *rgbSetNext[which][i]; // wenn aktueller Wert minus �nderungswert unter angepeiltem Wert herausk�men
					else *rgbSetActu[which][i] -= changeOfColor;
				}
				else if (*rgbSetActu[which][i] < *rgbSetNext[which][i]) {	// rgb nimmt zu // if eig �berfl�ssig
					if ((*rgbSetActu[which][i] + changeOfColor) > *rgbSetNext[which][i]) *rgbSetActu[which][i] = *rgbSetNext[which][i];
					else *rgbSetActu[which][i] += changeOfColor;
				}
				--amountOfSteps[which][i];		// runs-counter -1
				--actualState[which][i];		// anteil-durchl�ufe-counter -1
			}
		}
		longTimeDimStep[which] = 10;
	}
	return returnBool;
}

void InsosLedFunctions::xHelperDim_ModeEveryTenth(byte which) {	// extends xHelperLongTimeDimFromToExec

	stepValueTemp[which] = (3 * longTimeDimStep[which]);		// nicht genutzten Helper nutzen, hochrechnen auf aktuellen step

	while (stepValueTemp[which] > 9) stepValueTemp[which] -= 10;		// nach Angleichen durch longTimeDimStep zurechtstutzen auf nutzbaren Wert

	unsigned int tempVal = stepValueTemp[which];

	while (tempVal <= endOfStripe[which]) {
		if (tempVal >= startOfStripe[which]) leds[tempVal].setRGB(redActu[which], greenActu[which], blueActu[which]);
		tempVal += 10;
	}
	--longTimeDimStep[which];

}

void InsosLedFunctions::xHelperDim_ModeEverySecond(byte which) {	// extends xHelperLongTimeDimFromToExec

	unsigned int tempVal;
	for (byte r = 0; r < 5; ++r) {
		if (longTimeDimStep[which] == 10) tempVal = 0 + (2 * r);
		else tempVal = 1 + (2 * r);
		while (tempVal <= endOfStripe[which]) {
			if (tempVal >= startOfStripe[which]) leds[tempVal].setRGB(redActu[which], greenActu[which], blueActu[which]);
			tempVal += 10;
		}
	}
	longTimeDimStep[which] -= 5;
}

void InsosLedFunctions::xHelperDim_ModeWholeStripe(byte which) {
	for (int r = startOfStripe[which]; r <= endOfStripe[which]; ++r) leds[r].setRGB(redActu[which], greenActu[which], blueActu[which]);
	//fill_solid(leds + startOfStripe[which], endOfStripe[which], CRGB(redActu[which], greenActu[which], blueActu[which]));
	longTimeDimStep[which] = 0;
}
// nur aus Funktion aufzurufen
byte InsosLedFunctions::xHelperGetChangeValue(byte whichColor, byte whichValue = 0, byte whichStripe = 0)
// Color: 0 = brightness, 1 = rot, 2 = gr�n, 3 = blau, 4 = Wert von whichValue
// whichValue = value sent with function call
{
	byte comparat = 0;
	if (whichColor == 0) comparat = brightActu;
	else if (whichColor == 1) comparat = redActu[whichStripe];
	else if (whichColor == 2) comparat = greenActu[whichStripe];
	else if (whichColor == 3) comparat = blueActu[whichStripe];
	else if (whichColor == 4) comparat = whichValue;

	byte change;
	if (comparat < 10) change = 1;
	else if (comparat < 20) change = 2;
	else if (comparat < 46) change = 3;
	else if (comparat < 73) change = 4;
	else if (comparat < 158) change = 5;
	else if (comparat < 182) change = 6;
	else if (comparat < 239) change = 7;
	else change = 8;
	return change;
}



void InsosLedFunctions::reiheNachAnVorwaertsJedeFuerSichInitial(byte which, byte isLauflicht = 0) {
	#ifndef DEBUG
	laufLichtStepsLimit[which] = (endOfStripe[which] + 6);
	laufLichtStepsCount[which] = startOfStripe[which];
	startOfStripeLoop[which] = startOfStripe[which];
	endOfStripeLoop[which] = endOfStripe[which];
	if (isLauflicht == 0) {
		anyMillisProgramRunning[which] = 5;
		stepNow[which] = 10;
		xHelperWriteNextToActu(which);
	}
	else if (isLauflicht == 1) anyMillisProgramRunning[which] = 7;
	else if (isLauflicht == 2) anyMillisProgramRunning[which] = 9;
	#endif
}

void InsosLedFunctions::reiheNachAnRueckwaertsJedeFuerSichInitial(byte which, byte isLauflicht = 0) {
	#ifndef DEBUG
	laufLichtStepsLimit[which] = startOfStripe[which] - 7;
	laufLichtStepsCount[which] = endOfStripe[which];
	startOfStripeLoop[which] = startOfStripe[which];
	endOfStripeLoop[which] = endOfStripe[which];
	if (isLauflicht == 0) {
		anyMillisProgramRunning[which] = 6;
		stepNow[which] = 10;
		xHelperWriteNextToActu(which);
	}
	else if (isLauflicht == 1) anyMillisProgramRunning[which] = 8;
	else if (isLauflicht == 2) anyMillisProgramRunning[which] = 10;
	#endif
}

void InsosLedFunctions::reiheNachAnVorwaertsGesamtesArrayInitial(byte isLauflicht = 0) {
	laufLichtStepsLimit[0] = (endOfStripe[ANZAHL_ABSCHNITTE_REAL - 1] + 6);
	laufLichtStepsCount[0] = startOfStripe[0];
	startOfStripeLoop[0] = startOfStripe[0];
	endOfStripeLoop[0] = endOfStripe[ANZAHL_ABSCHNITTE_REAL - 1];
	if (isLauflicht == 0) {
		for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) {
			stepNow[x] = 10;
			xHelperWriteNextToActu(x);
		}
		anyMillisProgramRunning[0] = 5;
	}
	else if (isLauflicht == 1) anyMillisProgramRunning[0] = 7;
	else if (isLauflicht == 2) anyMillisProgramRunning[0] = 9;
}

void InsosLedFunctions::reiheNachAnRueckwaertsGesamtesArrayInitial(byte isLauflicht = 0) {
	#ifndef DEBUG
	laufLichtStepsLimit[0] = -7;
	laufLichtStepsCount[0] = endOfStripe[ANZAHL_ABSCHNITTE_REAL - 1];
	startOfStripeLoop[0] = startOfStripe[0];
	endOfStripeLoop[0] = endOfStripe[ANZAHL_ABSCHNITTE_REAL - 1];
	if (isLauflicht == 0) {
		for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) {
			stepNow[x] = 10;
			xHelperWriteNextToActu(x);
		}
		anyMillisProgramRunning[0] = 6;
	}
	else if (isLauflicht == 1) anyMillisProgramRunning[0] = 8;
	else if (isLauflicht == 2) anyMillisProgramRunning[0] = 10;
	#endif
}
// lauflicht bedeutet am Ende alles auf 0, ansonsten bleibt auf �bergebenen Werten an
void InsosLedFunctions::reiheNachAnVorwaertsLoop(byte which, byte isLauflicht = 0)
{
	int r, g, b;
	if (laufLichtStepsCount[which] <= endOfStripeLoop[which] && laufLichtStepsCount[which] >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which]] = CRGB((int)(redNext[which] * 0.1 + 0.5), (int)(greenNext[which] * 0.1 + 0.5), (int)(blueNext[which] * 0.1 + 0.5));
	if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 1 && laufLichtStepsCount[which] - 1 >= startOfStripeLoop[which])	leds[laufLichtStepsCount[which] - 1] = CRGB((int)(redNext[which] * 0.5 + 0.5), (int)(greenNext[which] * 0.5 + 0.5), (int)(blueNext[which] * 0.5 + 0.5));
	if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 2 && laufLichtStepsCount[which] - 2 >= startOfStripeLoop[which])	leds[laufLichtStepsCount[which] - 2] = CRGB(redNext[which], greenNext[which], blueNext[which]);

	if (isLauflicht == 0) {	// falls die Lampen an bleiben sollen mit Erh�hung
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 3 && laufLichtStepsCount[which] - 3 >= startOfStripeLoop[which]) {
			r = ((int)(redNext[which] * 1.25 + 0.5));
			g = ((int)(greenNext[which] * 1.25 + 0.5));
			b = ((int)(blueNext[which] * 1.25 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] - 3] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 4 && laufLichtStepsCount[which] - 4 >= startOfStripeLoop[which]) {
			r = ((int)(redNext[which] * 1.5 + 0.5));
			g = ((int)(greenNext[which] * 1.5 + 0.5));
			b = ((int)(blueNext[which] * 1.5 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] - 4] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 5 && laufLichtStepsCount[which] - 5 >= startOfStripeLoop[which]) {
			r = ((int)(redNext[which] * 1.25 + 0.5));
			g = ((int)(greenNext[which] * 1.25 + 0.5));
			b = ((int)(blueNext[which] * 1.25 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] - 5] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 6 && laufLichtStepsCount[which] - 6 >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which] - 6] = CRGB(redNext[which], greenNext[which], blueNext[which]);
	}
	else if (isLauflicht > 0) {	// falls danach die Lampen wieder aus gehen sollen oder die alte Farbe wieder hergestellt werden soll
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 3 && laufLichtStepsCount[which] - 3 >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which] - 3] = CRGB((int)(redNext[which] * 0.6 + 0.5), (int)(greenNext[which] * 0.6 + 0.5), (int)(blueNext[which] * 0.6 + 0.5));
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 4 && laufLichtStepsCount[which] - 4 >= startOfStripeLoop[which])	leds[laufLichtStepsCount[which] - 4] = CRGB((int)(redNext[which] * 0.3 + 0.5), (int)(greenNext[which] * 0.3 + 0.5), (int)(blueNext[which] * 0.3 + 0.5));
		if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 5 && laufLichtStepsCount[which] - 5 >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which] - 5] = CRGB((int)(redNext[which] * 0.1 + 0.5), (int)(greenNext[which] * 0.1 + 0.5), (int)(blueNext[which] * 0.1 + 0.5));
	}
	if (isLauflicht == 1) if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 6 && laufLichtStepsCount[which] - 6 >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which] - 6] = CRGB(0, 0, 0);
	if (isLauflicht == 2) if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 6 && laufLichtStepsCount[which] - 6 >= startOfStripeLoop[which]) leds[laufLichtStepsCount[which] - 6] = CRGB(redActu[which], greenActu[which], blueActu[which]);
	if (isLauflicht == 3) if (laufLichtStepsCount[which] <= endOfStripeLoop[which] + 6 && laufLichtStepsCount[which] - 6 >= startOfStripeLoop[which]) for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) if (laufLichtStepsCount[which] >= (int)startOfStripe[x] && laufLichtStepsCount[which] <= (int)endOfStripe[x]) leds[laufLichtStepsCount[which] + 6] = CRGB(redActu[x], greenActu[x], blueActu[x]);
	++laufLichtStepsCount[which];
	if (laufLichtStepsCount[which] > laufLichtStepsLimit[which]) resetLoopBools(which);
}
/* lauflicht :
0 : am Ende bleibt alles beim �bergebenen Farbwert
1 : am Ende ist die Stripe aus
2 : darf nur mit einzelnen Stripes aufgerufen werden -> alte Farbe wird wieder hergestellt
3 : darf nur mit allen Stripes aufgerfen werden - > alte Farbe wird wieder hergestellt
*/
void InsosLedFunctions::reiheNachAnRueckwaertsLoop(byte which, byte isLauflicht = 0) {
	#ifndef DEBUG
	int r, g, b;
	if (laufLichtStepsCount[which] >= startOfStripeLoop[which] && laufLichtStepsCount[which] <= endOfStripeLoop[which])	leds[laufLichtStepsCount[which]] = CRGB((int)(redNext[which] * 0.1 + 0.5), (int)(greenNext[which] * 0.1 + 0.5), (int)(blueNext[which] * 0.1 + 0.5));
	if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 1 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 1)	leds[laufLichtStepsCount[which] + 1] = CRGB((int)(redNext[which] * 0.5 + 0.5), (int)(greenNext[which] * 0.5 + 0.5), (int)(blueNext[which] * 0.5 + 0.5));
	if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 2 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 2)	leds[laufLichtStepsCount[which] + 2] = CRGB(redNext[which], greenNext[which], blueNext[which]);

	if (isLauflicht == 0) {	// falls die Lampen an bleiben sollen mit Erh�hung
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 3 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 3) {	// damit rgb-Werete nicht �ber 255 kommen k�nnen
			r = ((int)(redNext[which] * 1.25 + 0.5));
			g = ((int)(greenNext[which] * 1.25 + 0.5));
			b = ((int)(blueNext[which] * 1.25 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] + 3] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 4 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 4) {
			r = ((int)(redNext[which] * 1.5 + 0.5));
			g = ((int)(greenNext[which] * 1.5 + 0.5));
			b = ((int)(blueNext[which] * 1.5 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] + 4] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 5 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 5) {
			r = ((int)(redNext[which] * 1.25 + 0.5));
			g = ((int)(greenNext[which] * 1.25 + 0.5));
			b = ((int)(blueNext[which] * 1.25 + 0.5));
			if (r > 255) r = 255;
			if (g > 255) g = 255;
			if (b > 255) b = 255;
			leds[laufLichtStepsCount[which] + 5] = CRGB(r, g, b);
		}
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 6 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 6) leds[laufLichtStepsCount[which] + 6] = CRGB(redNext[which], greenNext[which], blueNext[which]);
	}
	if (isLauflicht > 0) {	// falls danach die Lampen wieder aus gehen sollen oder die alte Farbe wieder hergestellt werden soll
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 3 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 3) leds[laufLichtStepsCount[which] + 3] = CRGB((int)(redNext[which] * 0.6 + 0.5), (int)(greenNext[which] * 0.6 + 0.5), (int)(blueNext[which] * 0.6 + 0.5));
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 4 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 4)	leds[laufLichtStepsCount[which] + 4] = CRGB((int)(redNext[which] * 0.3 + 0.5), (int)(greenNext[which] * 0.3 + 0.5), (int)(blueNext[which] * 0.3 + 0.5));
		if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 5 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 5) leds[laufLichtStepsCount[which] + 5] = CRGB((int)(redNext[which] * 0.1 + 0.5), (int)(greenNext[which] * 0.1 + 0.5), (int)(blueNext[which] * 0.1 + 0.5));
	}
	if (isLauflicht == 1) if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 6 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 6) leds[laufLichtStepsCount[which] + 6] = CRGB(0, 0, 0);
	if (isLauflicht == 2) if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 6 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 6) leds[laufLichtStepsCount[which] + 6] = CRGB(redActu[which], greenActu[which], blueActu[which]);
	if (isLauflicht == 3) if (laufLichtStepsCount[which] >= startOfStripeLoop[which] - 6 && laufLichtStepsCount[which] <= endOfStripeLoop[which] - 6) for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) if (laufLichtStepsCount[which] >= (int)startOfStripe[x] && laufLichtStepsCount[which] <= (int)endOfStripe[x]) leds[laufLichtStepsCount[which] + 6] = CRGB(redActu[x], greenActu[x], blueActu[x]);
	--laufLichtStepsCount[which];
	if (laufLichtStepsCount[which] <= laufLichtStepsLimit[which]) resetLoopBools(which);
	#endif
}



void InsosLedFunctions::resetLoopBools(byte which) {
	anyMillisProgramRunning[which] = 0;
}


unsigned long InsosLedFunctions::xHelperCalcMinutesToMs(byte delayInMinutes) {
	return (delayInMinutes * 60000);
}



// setter helper

void InsosLedFunctions::xHelperSetActu(byte r, byte g, byte b, byte which, bool oneByOneInProgress = 0)
{
	redActu[which] = r;

	greenActu[which] = g;

	blueActu[which] = b;

	/*if (oneByOneInProgress == 0) {
		if (r != 0 || b != 0 || g != 0) {
			stepNow[which] = 10;
			stepNow[0] = 10;
		}
		else {
			stepNow[which] = 0;
			stepNow[0] = 0;
		}
	}*/
}

void InsosLedFunctions::xHelperSetNext(byte r, byte g, byte b, byte which)
{
	redNext[which] = r;
	greenNext[which] = g;
	blueNext[which] = b;
}

void InsosLedFunctions::xHelperWriteNextToActu(byte which)
{
	redActu[which] = redNext[which];
	greenActu[which] = greenNext[which];
	blueActu[which] = blueNext[which];

	/*if (redActu[which] != 0 || greenActu[which] != 0 || blueActu[which] != 0) {
		stepNow[which] = 10;
		stepNow[0] = 10;
	}
	else {
		stepNow[which] = 0;
		stepNow[0] = 0;
	}*/
}

void InsosLedFunctions::xHelperWriteActuToNext(byte which)
{
	redNext[which] = redActu[which];
	greenNext[which] = greenActu[which];
	blueNext[which] = blueActu[which];
}





// program modes text device
/*
Wichtig !! Aufgerufen wird mit which 0 alle, danach einzelne Stripes. Des weiteren wird aber mit which 0 1 2 f�r einzelne Stripes gerechnet ,damit es mit den Arrays passt!!
*/

void InsosLedFunctions::programJustOff(byte which) {
	if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 1, 0, 0, 0);
	else callAnyProgram(which, 1, 0, 0, 0);
}

void InsosLedFunctions::programDimmToDefault(byte which) {
	if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 1, RED_DEFAULT, GREEN_DEFAULT, BLUE_DEFAULT);
	else callAnyProgram(which, 1, RED_DEFAULT, GREEN_DEFAULT, BLUE_DEFAULT);
}

void InsosLedFunctions::programDimTo(byte which, byte red, byte green, byte blue) {
	if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 1, red, green, blue);
	else callAnyProgram(which, 1, red, green, blue);
}

void InsosLedFunctions::programDimToEverySecond(byte which, byte red, byte green, byte blue) {
}

void InsosLedFunctions::programWakeUpLight(byte which, byte red, byte green, byte blue, byte delayTimeMinutes) {
	if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 2, red, green, blue, delayTimeMinutes);
	else callAnyProgram(which, 2, red, green, blue, delayTimeMinutes);
}

void InsosLedFunctions::programSleepLight(byte which, byte red, byte green, byte blue, byte delayTimeMinutes) {
	if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 3, red, green, blue, delayTimeMinutes);
	else callAnyProgram(which, 3, red, green, blue, delayTimeMinutes);
}
/*
mode 1 = reihe nach an einzelne Stripe, bleibt an
mode 2 = kleines Lauflicht, danach aus
mode 3 = kleines Lauflicht, danach _alte_ Farbe
*/
void InsosLedFunctions::programReiheNachAnSeparateStripe(byte which, byte red, byte green, byte blue, byte mode, bool rueckwaerts) {
	if (mode == 1) {
		if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 41 + rueckwaerts, red, green, blue);
		else callAnyProgram(which, 41 + rueckwaerts, red, green, blue);
	}
	else if (mode == 2) {
		if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 45 + rueckwaerts, red, green, blue);
		else callAnyProgram(which, 45 + rueckwaerts, red, green, blue);
	}
	else if (mode == 3) {
		if (which == 99) for (byte x = 0; x < anzahlAbschnitte; ++x) callAnyProgram(x, 49 + rueckwaerts, red, green, blue);
		else callAnyProgram(which, 49 + rueckwaerts, red, green, blue);
	}
}

void InsosLedFunctions::programReiheNachAnGesamtesArray(byte which, byte red, byte green, byte blue, byte mode, bool rueckwaerts) {
	if (mode == 1) callAnyProgram(0, 43 + rueckwaerts, red, green, blue);
	if (mode == 2) callAnyProgram(0, 47 + rueckwaerts, red, green, blue);
	if (mode == 3) callAnyProgram(0, 51 + rueckwaerts, red, green, blue);
}

#ifdef DEBUG
void InsosLedFunctions::xHelperPrintActu(byte which) {
	Serial.println(" ");
	Serial.print("Farbwerte Actu which ");
	Serial.print(which);
	Serial.print(" : ");
	Serial.print(redActu[which]);
	Serial.print(" , ");
	Serial.print(greenActu[which]);
	Serial.print(" , ");
	Serial.println(blueActu[which]);
}

void InsosLedFunctions::xHelperPrintNext(byte which) {
	Serial.println(" ");
	Serial.print("Farbwerte Next which ");
	Serial.print(which);
	Serial.print(" : ");
	Serial.print(redNext[which]);
	Serial.print(" , ");
	Serial.print(greenNext[which]);
	Serial.print(" , ");
	Serial.println(blueNext[which]);
	Serial.println(" ");
}

void InsosLedFunctions::xHelperPrintSteps(byte which) {
	Serial.println(" ");
	Serial.print("Stepwerte which ");
	Serial.print(which);
	Serial.println(" : ");
	Serial.print("StepNow = ");
	Serial.print(stepNow[which]);
	Serial.println(" , ");
	Serial.print("Stelle im Zaehler ohne Schaltung = ");
	Serial.print(byteToDelayDim[which]);
	//Serial.print(" , ");
}

void InsosLedFunctions::xHelperPrintDelayCalculation(byte x, bool inclStepUpDown, byte which, unsigned long delayTimeInMiliSeconds) {
	if (inclStepUpDown) {
		// Serial.println("millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / ((amountOfSteps[which][2] * 10) + ((endOfStripe[which] - startOfStripe[which]) * 10)); ");
		Serial.print(millisDelayTimeDefault[which]);
		Serial.print(" = ");
		Serial.print(delayTimeInMiliSeconds);
		Serial.print(" / ( ");
		Serial.print(amountOfSteps[which][x]);
		Serial.print("  *10 ) + ( ( ");
		Serial.print(endOfStripe[which] + 1);
		Serial.print(" - ");
		Serial.print(startOfStripe[which]);
		Serial.println(" ) *10 )");
	}
	else {
		// Serial.println("millisDelayTimeDefault[which] = (delayTimeInMiliSeconds) / ((amountOfSteps[which][2] * 10)); "); 
		Serial.print(millisDelayTimeDefault[which]);
		Serial.print(" = ");
		Serial.print(delayTimeInMiliSeconds);
		Serial.print(" / ");
		Serial.print(amountOfSteps[which][x]);
		Serial.print("  *10");
	}
}

#endif