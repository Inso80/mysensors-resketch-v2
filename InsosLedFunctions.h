// InsosLedFunctions.h

#ifndef _INSOSLEDFUNCTIONS_h
#define _INSOSLEDFUNCTIONS_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "config.h"

#include <FastLED.h>			// FastLed Libary
// #include <MySensors.h>			// MySensors Libary



class InsosLedFunctions {

private:

	CRGB leds[NUM_LEDS];		// LED array

	// wake / sleeplight variables
	boolean longTimeDimUpFromToAlreadyRunning;			// wakeUpLight aktiv
	boolean longTimeDimDownFromToAlreadyRunning;		// sleepLight aktiv

	byte whichLongTimeDim = 0;		// �bergabe von which(stripe) an Funktionen die mit millis rennen.

	/** hier wird festgehalten ob auf 1 oder 0 runtegedimmt wird, bevor die einzelnen LEDs dann in anderer Fuinktion abgeschaltet werden
	auch: welche Farben auf next �bergangsweise gesetzt werden, w�hrend beim Hochfahren einzelne LEDs eingeschaltet werden
	wichtig ist hier das nur Farben mitkommen die wenigstens halb so hell sind wie die hellste Farbe
	*/
	byte dimDownTempValues[3];	// vorher unbedingt xHelperDimDownPreCalc 



	// - - - - - - - - - - - - - - - - - 
	// neu geschrieben bzw �berarbeitet
	// - - - - - - - - - - - - - - - - - 

// bright-Werte. Werden im Normalfall nicht ver�ndert, da sie f�r ganzes LED Array gelten
	byte brightActu;
	byte brightNext;

	byte anzahlAbschnitte = ANZAHL_ABSCHNITTE_REAL;

	byte* anyMillisProgramRunning;			// Abfrage ob (irgend)ein Programm rennt, falls nicht gar keine Abfrage von millis

	// gr��e der Arrays entsprechend der Anzahl der Abschnitte pro 
	byte redActu[3];
	byte greenActu[3];
	byte blueActu[3];

	byte redNext[3];
	byte greenNext[3];
	byte blueNext[3];

	unsigned long * millisDelayTimeDefault;  //the value is a number of minutes, gets changed to ms within the method !
	unsigned long * startMillis;

	unsigned int startOfStripe[3];		// start-und endpunkte so das man direkt mit von zu arbeiten kann
	unsigned int endOfStripe[3];

	// Bereich LedsOnOneByOne bzw Off
	byte stepNow[3];		// Stelle, an der sich der Schritt ggf gerade befindet (on und off one by one).Falls 0 sind LEDs aus, falls 10 sind alle LEDs an
	int stepValueTemp[3];		// Bereich der gerade bei LedsEinzelnOn oder Off hochgerechnet wird, wird nur tempor�r f�r den Durchlauf gebraucht (alle mit 3,6,9,2,5 usw), int weil negative Werte ben�tigt werden, f�r on und off one by one
	unsigned int  singleLedWhichActual[3];	// sagt exakt die LED die gerade geschaltet wurde
	byte byteToDelayDim[3];		// byte das hochgez�hlt wird, und bei DELAY_DIM_VERZOEGERUNG_RUNS dann einmal dimmen zu lassen. Wird als Delay eingesetzt

	unsigned int  amountOfSteps[3][3];						// Schritte die n�tig sind um jede Farbe von actu zu next zu bekommen, Berechnung je nach Helligkeit daher komplitzierter
	float counterForFullsteps[3][3];					// Farbwertangleich beim Dimmen. Hellste Farbe ist eins, andere Farben haben einen anteiligen Wert. 0,5 bedeutet bspw nur jeder Zweite Step, da die farbe auch nur halb so hell ist
	float actualState[3][3];						// Schrittz�hler der runtergerechnet wird

	byte * rgbSetActu[3][3];	// Array mit Pointern auf aktuelle Farbwerte
	byte * rgbSetNext[3][3];	// Array mit Pointern auf zu setzende Farbwerte

	unsigned int longTimeDimStep[3];	// f�r die lange dim- Kette. Merkt sich die Position die dann die einzuschaltenden LEDs angibt. _Muss_ immer mit 10 arbeiten!

	int laufLichtStepsCount[3];	// "das ++x" also der laufende Wert - WICHTIG ! INTEGER ! R�ckw�rts geht in�s negative

	int laufLichtStepsLimit[3];	// da das ganze auch manchmal mittendrin (weil Stripe nicht bei 0 beginnt bzw je nach Richtung bei NUM_LEDS endet, wird hier das Ende festgelegt - WICHTIG ! INTEGER ! R�ckw�rts geht in�s negative

	unsigned int  startOfStripeLoop[3];

	unsigned int  endOfStripeLoop[3];

	byte dimTempValues[3];	// vorher unbedingt xHelperEinzelnOnOffRgbPreCalc 

	byte emptyStepsOnOffDelay[3];



	// private functions

	void createArraysOnObjectCreation();

	void callAnyProgram(byte which, byte programMode, byte red, byte green, byte blue, unsigned long dimTime, bool additionalBool = 0);

	void xHelperCalcDefaultDelayTimePerStep(byte which, unsigned long delayTimeInMiliSeconds, byte inclStepUpDown, bool longOne = 0, byte dimStepsPerRun = 2);

	void xHelperCalcDefaultDelayTimeHelperOfHelper(byte which, unsigned long delayTimeInMiliSeconds, byte inclStepUpDown, bool longOne, byte whichIsBiggest, byte dimStepsPerRun);

	void xHelperEinzelnOnOffRgbPreCalc(byte whichStripe, boolean checkForRgbNext = 0);


	// LedsOnOffOneByOne-Krams
	void ledsOnOneByOneInitial(byte which, bool longOne = 0, byte emptyStepsOnOffDelayIfNotNull = 0);

	void ledsOnOneByOneLongLoop(byte which);

	void ledsOnOneByOneShortLoop(byte which);


	void ledsOffOneByOneInitial(byte which, byte longOne = 0, byte emptyStepsOnOffDelayIfNotNull = 0);

	void ledsOffOneByOneLongLoop(byte which = 0);

	void ledsOffOneByOneShortLoop(byte which);


	void dimFromToInitial_ModeWholeStripe(byte which);

	void dimFromToInitial_ModeEverySecond(byte which);

	void dimFromToInitial_ModeEveryTenth(byte which);

	void dimFromToInitialWhenOffOneByOneFollows_ModeWholeStripe(byte which);

	void dimFromToInitialWhenOffOneByOneFollows_ModeEverySecond(byte which);

	void dimFromToInitialWhenOffOneByOneFollows_ModeEveryTenth(byte which);

	bool xHelperSetLedFromTo(byte which, boolean every = 1);

	void xHelperDim_ModeEveryTenth(byte which);

	void xHelperDim_ModeEverySecond(byte which);

	void xHelperDim_ModeWholeStripe(byte which);

	byte xHelperGetChangeValue(byte whichColor, byte whichValue = 0, byte whichStripe = 0);


	void reiheNachAnVorwaertsJedeFuerSichInitial(byte which, byte isLauflicht = 0);

	void reiheNachAnRueckwaertsJedeFuerSichInitial(byte which, byte isLauflicht = 0);

	void reiheNachAnVorwaertsGesamtesArrayInitial(byte isLauflicht = 0);

	void reiheNachAnRueckwaertsGesamtesArrayInitial(byte isLauflicht = 0);

	void reiheNachAnVorwaertsLoop(byte which, byte isLauflicht = 0);

	void reiheNachAnRueckwaertsLoop(byte which, byte isLauflicht = 0);


	void resetLoopBools(byte which);


	unsigned long xHelperCalcMinutesToMs(byte delayInMinutes);



	void xHelperSetActu(byte r, byte g, byte b, byte which, bool oneByOneInProgress = 0);

	void xHelperSetNext(byte r, byte g, byte b, byte which);

	void xHelperWriteNextToActu(byte which);

	void xHelperWriteActuToNext(byte which);


	#ifdef DEBUG
	void xHelperPrintActu(byte which);

	void xHelperPrintNext(byte which);

	void xHelperPrintSteps(byte which);
	#endif

	void xHelperPrintDelayCalculation(byte x, bool inclStepUpDown, byte which, unsigned long delayTimeInMiliSeconds);



public:

	InsosLedFunctions(byte * anyMillisProgramRunning, unsigned long * millisDelayTimeDefault, unsigned long * startMillis);

	void runAnyProgram(byte which);

	void programJustOff(byte which);

	void programDimmToDefault(byte which);

	void programDimTo(byte which, byte red, byte green, byte blue);

	void programDimToEverySecond(byte which, byte red, byte green, byte blue);

	void programWakeUpLight(byte which, byte red, byte green, byte blue, byte delayTimeMinutes);

	void programSleepLight(byte which, byte red, byte green, byte blue, byte delayTimeMinutes);

	void programReiheNachAnSeparateStripe(byte which, byte red, byte green, byte blue, byte mode, bool rueckwaerts);

	void programReiheNachAnGesamtesArray(byte which, byte red, byte green, byte blue, byte mode, bool rueckwaerts);

};

#endif