/*
	Name:       Node41_SchlZim_lang_Wand_A.ino
	Created:	16.02.2019 19:23:22
	Author:     INSO-MAINFRAME\Inso
*/

// Enable debug prints
// #define MY_DEBUG

#include "config.h"

#include <MySensors.h>			// MySensors Libary

#include "InsosLedFunctions.h"

// millis f�r wakeup/ sleeplight, hier f�r millis statt delay da lange Zeitabst�nde

// Initialisierung f�r MySensors
MyMessage msgLedRgb(CHILD_ID_LED_RGB, V_RGB);
MyMessage msgLedDim(CHILD_ID_LED_DIMMER, V_DIMMER);
MyMessage msgText(CHILD_ID_TEXT, V_TEXT);

InsosLedFunctions * myLeds;

char receivedText[26] = "";		// text array um den string der zum text device geschickt wird zwischenzuspeichern und auszuwerten

/*
Wichtig, folgende Werte f�r folgende Abfolgen:
0	ledsOnOneByOne
1	ledsOffOneByOne
*/
byte anyMillisProgramRunningMain[]{ 0,0,0 };

bool anyMillisExecuted = 0;

unsigned long millisDelayTimeDefaultMain[3];  // gets changed every call, in ms, value for every step !
unsigned long startMillisMain[3];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// Standardfunktionen
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void setup()
{
	#ifndef NUM_LEDS_PT_TWO
	#define NUM_LEDS_PT_TWO 0
	#endif

	#ifdef LEDS2812
	#define INIT_MODE 1
	#elif LEDS2812_ZWEI_PINS
	#define	INIT_MODE 2
	#elif LEDS2811_ZWEI_PINS
	#define	INIT_MODE 12
	#endif

	// ansonsten init mode in der Lib adden

	myLeds = new InsosLedFunctions(anyMillisProgramRunningMain, millisDelayTimeDefaultMain, startMillisMain);
	
	#ifndef DEBUG
	myLeds->programReiheNachAnGesamtesArray(0, 0, 150, 0, 2, 0);
	#endif

	#ifdef DEBUG
	Serial.begin(115200);
	myLeds->programReiheNachAnGesamtesArray(0, 150, 0, 0, 2, 0);
	#endif

	send(msgText.setSensor(CHILD_ID_TEXT).set("0,0,0,0,0"));	// text wird auf 0 gesetzt da Stripes ja jetzt aus sind
}


void presentation()
{
	sendSketchInfo(SKETCH_INFO_NAME, SKETCH_INFO_VERSION);
	present(CHILD_ID_LED_RGB, S_RGB_LIGHT, RGB_LIGHT_NAME);
	present(CHILD_ID_LED_DIMMER, S_DIMMER, S_DIMMER_NAME);
	present(CHILD_ID_TEXT, S_INFO, S_INFO_NAME);

	#ifdef DEBUG
	Serial.println("- - - GO - - -");
	#endif
}


void loop()
{
	// Programme die laufen bis der boolsche "soll laufen" wert wieder 0 ist, daf�r immer als erste anyprogramRunning 1 setzen
	for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) {
		if (anyMillisProgramRunningMain[x] != 0) {
			if ((startMillisMain[x] + millisDelayTimeDefaultMain[x]) <= millis()) {
				myLeds->runAnyProgram(x);
				startMillisMain[x] = millis();
				anyMillisExecuted = 1;
			}
		}
	}
	if (anyMillisExecuted) {
		FastLED.show();
		anyMillisExecuted = 0;
	}
	// Serial.println(freeMemory());
}


void receive(const MyMessage &message) {
	//// int ID = message.sensor;
	//#ifdef DEBUG
	//Serial.println("");
	//Serial.println(" - - - - - - - - - ");
	//Serial.println("");
	//Serial.print("Sensor: ");
	//Serial.println(message.sensor);
	//Serial.print("Data: ");
	//Serial.println(message.data);
	//Serial.print("SensorType: ");
	//Serial.println(message.sensorType);
	//Serial.print("Type: ");
	//Serial.println(message.type);
	//Serial.println("");
	//// Serial.println(" -   -   -   -   - ");
	//Serial.println("");
	//#endif

	byte redTemp;
	byte greenTemp;
	byte blueTemp;

	switch (message.sensor) {
	case CHILD_ID_LED_RGB:		// Device in Domoticz das RGB als Hex sendet bzw 1 und 0 f�r ein und aus sendet

		//myLeds->xHelperSetAllProgramZero();
		if (*message.data == '0' && !message.data[1]) {
			// myLeds->callRgbLampsOut();
		}
		else if (message.data[0] == '1' && !message.data[1])
		{
			// myLeds->callRgbLampsOn();
		}
		else
		{
			/*#ifdef DEBUG
			Serial.println("Lamps to !");
			#endif
			for (byte aP = 0; aP < 6;)
			{
				byte x = 0;
				if (message.data[aP] >= '0' && message.data[aP] <= '9') x += message.data[aP] - '0';
				else if (message.data[aP] >= 'A' && message.data[aP] <= 'F') x += message.data[aP] - '7';
				x = x << 4;
				++aP;
				if (message.data[aP] >= '0' && message.data[aP] <= '9') x += message.data[aP] - '0';
				else if (message.data[aP] >= 'A' && message.data[aP] <= 'F') x += message.data[aP] - '7';
				++aP;

				myLeds->callHelperSwitch(aP, x);
			}
			myLeds->callRgbLampsTo();*/
		}
		break;

	case CHILD_ID_LED_DIMMER:	// Domoticz-Dimmer, aktuell noch nicht genauer festgelegt
		// myLeds->xHelperSetAllProgramZero();
		break;

	case CHILD_ID_TEXT:			// das Text-Device. dzVents nutzt i.d.R. dieses Device
		// myLeds->xHelperSetAllProgramZero();
		byte programMode;	// which program to start
		byte which;			// which LED part to act on 0 = alle, 1 = vor Kopf, 2 = seitlich
		snprintf(receivedText, sizeof(receivedText), "%16s", message.getString());
		programMode = atoi(strtok(receivedText, ","));
		which = atoi(strtok(NULL, ","));
		redTemp = atoi(strtok(NULL, ","));
		greenTemp = atoi(strtok(NULL, ","));
		blueTemp = atoi(strtok(NULL, ","));

		if (which == 0) which = 99;		// da die Stripes im LED Sketch bei 0 beginnen, in der �bertragung die 0 aber f�r alle steht, hier die "Umrechnung"
		else --which;

		if (which == 99) for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) anyMillisProgramRunningMain[x] = 0;
		else anyMillisProgramRunningMain[which] = 0;
		// startmillis ganz unten

		// if (which > (sizeof(startOfStripe) / sizeof(startOfStripe[0])))

		if (programMode == 0) {			// "bestimmte" oder alle Stripes off incl runterdimmen und einzeln off
			myLeds->programJustOff(which);
		}
		else if (programMode == 1) {	// Lampen auf default hoch (oder runter)dimmen
			myLeds->programDimmToDefault(which);
			Serial.println("Program 2");
		}
		else if (programMode == 2) {	// Lampen auf �bergebenen Wert hoch (oder runter)dimmen
			myLeds->programDimTo(which, redTemp, greenTemp, blueTemp);
		}
		else if (programMode == 3) {	// Lampen auf �bergebenen Wert hoch (oder runter)dimmen, nur jede zweite an
			myLeds->programDimToEverySecond(which, redTemp, greenTemp, blueTemp);
		}
		else if (programMode == 10) {	// Lauflicht f�r Bewegungsmelder, nutzen festen Wert. richtung LED 0 zu max
			// myLeds->reiheNachAn(which, 0);
		}
		else if (programMode == 11) {	// Lauflicht f�r Bewegungsmelder, nutzen festen Wert. richtung LED max zu 0
			// myLeds->reiheNachAn(which, 1);
		}
		else if (programMode == 16) {	// das wakeUpLight
			byte delayTimeMinutes = atoi(strtok(NULL, ","));
			myLeds->programWakeUpLight(which, redTemp, greenTemp, blueTemp, delayTimeMinutes);
		}
		else if (programMode == 17) {		// sleep light
			byte delayTimeMinutes = atoi(strtok(NULL, ","));
			myLeds->programSleepLight(which, redTemp, greenTemp, blueTemp, delayTimeMinutes);
		}

		// - - - - - - - 
		// ab hier Reihe nach an
		// - - - - - - - 

		else if (programMode == 41) {		// Reihe nach vorwaerts an immer nur eine bzw mehrere parallel, bleibt danach an
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 1, 0);
		}
		else if (programMode == 42) {		// Reihe nach r�ckw�rts an immer nur eine bzw mehrere parallel, bleibt danach an
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 1, 1);
		}
		else if (programMode == 43) {		// Reihe nach vorwaerts an, gesamtes Array, kein which, bleibt danach an
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 1, 0);
			which = 99;
		}
		else if (programMode == 44) {		// Reihe nach r�ckw�rts an, gesamtes Array, kein which, bleibt danach an
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 1, 1);
			which = 99;
		}


		else if (programMode == 45) {		// Reihe nach vorwaerts an immer nur eine bzw mehrere parallel, danach Stripe wieder aus -> Lauflicht 
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 2, 0);
		}
		else if (programMode == 46) {		// Reihe nach r�ckw�rts an immer nur eine bzw mehrere parallel, danach Stripe wieder aus -> Lauflicht 
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 2, 1);
		}
		else if (programMode == 47) {		// Reihe nach vorwaerts an, gesamtes Array, kein which, danach Stripe wieder aus -> Lauflicht 
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 2, 0);
			which = 99;
		}
		else if (programMode == 48) {		// Reihe nach r�ckw�rts an , gesamtes Array, kein which, danach Stripe wieder aus -> Lauflicht 
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 2, 1);
			which = 99;
		}


		else if (programMode == 49) {		// Reihe nach vorwaerts an immer nur eine bzw mehrere parallel, danach wieder alte Farbe -> Lauflicht 
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 3, 0);
		}
		else if (programMode == 50) {		// Reihe nach r�ckw�rts an immer nur eine bzw mehrere parallel, danach wieder alte Farbe -> Lauflicht 
			myLeds->programReiheNachAnSeparateStripe(which, redTemp, greenTemp, blueTemp, 3, 1);
		}
		else if (programMode == 51) {		// Reihe nach vorwaerts an, gesamtes Array, kein which, danach wieder alte Farbe -> Lauflicht 
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 3, 0);
			which = 99;
		}
		else if (programMode == 52) {		// Reihe nach r�ckw�rts an, gesamtes Array, kein which, danach wieder alte Farbe -> Lauflicht 
			myLeds->programReiheNachAnGesamtesArray(which, redTemp, greenTemp, blueTemp, 3, 1);
			which = 99;
		}

		// - - - - - - - 
		// ab hier 
		// - - - - - - - 

		if (which == 99) for (byte x = 0; x < ANZAHL_ABSCHNITTE_REAL; ++x) startMillisMain[x] = millis();
		else startMillisMain[which] = millis();
	}
};


