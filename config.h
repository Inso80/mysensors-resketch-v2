#pragma once


#define MY_RADIO_RF24			// MySensors Radio Definition

#define MY_NODE_ID 95			// 95 for test purposes - restliche NodeIDs aus Domoticz Hardware ableiten

/*
Nodes:
21 Arbeitszimmer (Motion) Decke
22 Arbeitszimmer Schrank
41 Bedroom lange Wand
42 Bedroom Unterbett
43 Bedroom beide Boxen
95 Testumgebung
*/

#define SKETCH_INFO_VERSION "1.03"

// #define DEBUG				// for debug purposes, enables serial.begin

#ifdef DEBUG
#pragma message (" ! WARNING !  Debug enabled  ! WARNING !")
// #include "MemoryFree.h"		// use with Serial.print("freeMemory()="); Serial.println(freeMemory());
#endif

// default-Werte die sowohl f�r Domoticz an, als auch f�r txt-System mode 01 verwendet werden
#define RED_DEFAULT 250			
#define GREEN_DEFAULT 70
#define BLUE_DEFAULT 0

#define DELAY_DIM_VERZOEGERUNG_RUNS	0		// hier�ber wird geregelt, wie viele Zwischenschritte beim runterdimmen als Filler dazukommen - Minimum 0, also aus !

// #define LEDS_ON_OFF_DELAY_TIME_DEFAULT 75		// die default zeit f�r ledsAnOneByOne und LedsOffOneByOne

// #define DIM_FROM_TO_DELAY_TIME_DEFAULT 75		// allgemeine DimmZeit allgemein

#define LAUF_LICHT_DELAY_TIME 5					// Lauflicht-Initialisierung Geschwindigkeit

// neu !
#define DIM_TIME_DEFAULT 400

//#define LEDS2812 


#if (MY_NODE_ID == 21)		// Arbeitszimmer (Motion) Decke Arduino
#define LEDS2811_ZWEI_PINS	// restlicher Aufbau gem�� https://github.com/FastLED/FastLED/wiki/Multiple-Controller-Examples
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define LED_PIN_2 7			// ggf zweiter Pin
#define ANZAHL_ABSCHNITTE_REAL 2	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 80			// Leds am Strang insgesamt bzw hier an ebiden Str�ngen zusammen
#define NUM_LEDS_PT_TWO	40	// Num Leds am ersten Pin (hier auch  am zweiten)
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 40 	// erster Bereich
#define SECOND_PART_LEDS 40 	// zweiter Bereich
#define THIRD_PART_LEDS 0		// dritter Bereich
// MySensors
#define DIGITAL_INPUT_SENSOR 2   // The digital input you attached your motion sensor.  (Only 2 and 3 generates interrupt!)
// IDs f�r MySensors presentation stuff
#define CHILD_ID 1   // Id of the sensor child
#define CHILD_ID_LED_RGB 2		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 3	// Id of LED Stripe
#define CHILD_ID_TEXT 4			// Id of Text child

#define SKETCH_INFO_NAME "Arbzimmer_Drucker01_A"
#define RGB_LIGHT_NAME "Vorhang LED Stripe"
#define S_DIMMER_NAME "Vorhang Dimmer"
#define S_INFO_NAME "Vorhang Text LED Module"
#define S_MOTION_NAME "Motion Decke Arbeitszimmer"
#endif

#if (MY_NODE_ID == 22)		// Arbeitszimmer Schrank
#define LEDS2812 
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define ANZAHL_ABSCHNITTE_REAL 3	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 95			// Leds am Strang insgesamt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 21 	// erster Bereich
#define SECOND_PART_LEDS 53 	// zweiter Bereich
#define THIRD_PART_LEDS 21		// dritter Bereich

// eigentlich 87 LEDs -> umbauen auf INT !

// MySensors
// IDs f�r MySensors presentation stuff
#define CHILD_ID_LED_RGB 1		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 2	// Id of LED Stripe
#define CHILD_ID_TEXT 3			// Id of Text child

#define SKETCH_INFO_NAME "ArbZim_Schrank_01 A"
#define RGB_LIGHT_NAME "AZ Schrank LED A@22"
#define S_DIMMER_NAME "AZ Schrank Dim A@22"
#define S_INFO_NAME "AZ Schrank LEDText A@22"
#endif

#if (MY_NODE_ID == 41)		// Bedroom lange Wand
#define LEDS2812 
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define ANZAHL_ABSCHNITTE_REAL 3	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 257			// Leds am Strang insgesamt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 87 	// erster Bereich
#define SECOND_PART_LEDS 83 	// zweiter Bereich
#define THIRD_PART_LEDS 87		// dritter Bereich

// eigentlich 87 LEDs -> umbauen auf INT !

// MySensors
// IDs f�r MySensors presentation stuff
#define CHILD_ID_LED_RGB 1		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 2	// Id of LED Stripe
#define CHILD_ID_TEXT 3			// Id of Text child

#define SKETCH_INFO_NAME "SchlZim lang Wand A"
#define RGB_LIGHT_NAME "SZ lang Wand LED A@41"
#define S_DIMMER_NAME "SZ lang Wand Dim A@41"
#define S_INFO_NAME "SZ lang Wand LEDText A@41"
#endif

#if (MY_NODE_ID == 42)		// Bedroom Unterbett
#define LEDS2812 
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define PIN_SENSOR_01 2		// Pin of MOTion Sensor 01
#define PIN_SENSOR_02 3		// Pin of MOTion Sensor 02
#define PIN_SENSOR_03 4		// Pin of MOTion Sensor 03
#define ANZAHL_ABSCHNITTE_REAL 2	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 168			// Leds am Strang insgesamt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 74 	// erster Bereich
#define SECOND_PART_LEDS 94 	// zweiter Bereich
#define THIRD_PART_LEDS 87		// dritter Bereich

// eigentlich 87 LEDs -> umbauen auf INT !

// MySensors
// IDs f�r MySensors presentation stuff
#define CHILD_ID_LED_RGB 1		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 2	// Id of LED Stripe
#define CHILD_ID_TEXT 3			// Id of Text child
#define CHILD_ID_MOT1 4		// Id of MOTion Sensor 01
#define CHILD_ID_MOT2 5		// Id of MOTion Sensor 02
#define CHILD_ID_MOT3 6		// Id of MOTion Sensor 03

#define SKETCH_INFO_NAME "SchlZim lang Wand A"
#define RGB_LIGHT_NAME "SZ lang Wand LED A@42"
#define S_DIMMER_NAME "SZ lang Wand Dim A@42"
#define S_INFO_NAME "SZ lang Wand LEDText A@42"
#endif


#if (MY_NODE_ID == 43)		// Bedroom Temp Boxen links rechts Arduino
#define LEDS2812_ZWEI_PINS
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define LED_PIN_2 7
#define ANZAHL_ABSCHNITTE_REAL 2	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 60			// Leds am Strang insgesamt
#define NUM_LEDS_PT_TWO	30	// Num Leds am ersten Pin (hier auch  am zweiten)
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 30 	// erster Bereich
#define SECOND_PART_LEDS 30 	// zweiter Bereich
#define THIRD_PART_LEDS 87		// dritter Bereich

// MySensors
// IDs f�r MySensors presentation stuff
#define CHILD_ID_LED_RGB 1		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 2	// Id of LED Stripe
#define CHILD_ID_TEXT 3			// Id of Text child

#define SKETCH_INFO_NAME "SchlZim Temp Box A"
#define RGB_LIGHT_NAME "SZ Temp Box LED A@43"
#define S_DIMMER_NAME "SZ Temp Box Dim A@43"
#define S_INFO_NAME "SZ Temp Box LEDText A@43"
#endif

#if (MY_NODE_ID == 95)		// Testumgebung
#define LEDS2812 
#define LED_PIN 6			// Pin of the 2812b LED stripe
#define ANZAHL_ABSCHNITTE_REAL 2	// wird sowohl in MySensors als auch LedFunctions genutzt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define NUM_LEDS 16			// Leds am Strang insgesamt
// Wichtig !! wenn NUM_LEDS > 255 UNBEDINGT startOfStripe und endOfStripe zu int setzen !!!
#define FIRST_PART_LEDS 8 	// erster Bereich
#define SECOND_PART_LEDS 8 	// zweiter Bereich
#define THIRD_PART_LEDS 0		// dritter Bereich
// MySensors
// IDs f�r MySensors presentation stuff
#define CHILD_ID_LED_RGB 1		// Id of LED Stripe
#define CHILD_ID_LED_DIMMER 2	// Id of LED Stripe
#define CHILD_ID_TEXT 3			// Id of Text child

#define SKETCH_INFO_NAME "Test01"
#define RGB_LIGHT_NAME "Test RGB LED Stripe"
#define S_DIMMER_NAME "Test RGB Dimmer"
#define S_INFO_NAME "Test Text LED Module"
#endif